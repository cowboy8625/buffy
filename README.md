# Buffy

[![docs.rs](https://docs.rs/buffy/badge.svg)](https://docs.rs/buffy)
[![crates.io](https://img.shields.io/crates/v/buffy.svg)](https://crates.io/crates/buffy)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE)
![lastupdated](https://img.shields.io/github/last-commit/cowboy8625/buffy)
![GitHub repo size](https://img.shields.io/github/repo-size/cowboy8625/buffy)
![issuse](https://img.shields.io/github/issues/cowboy8625/buffy)
![Discord](https://img.shields.io/discord/509849754155614230)
![Lines of Code](https://tokei.rs/b1/github/cowboy8625/buffy)

Buffy is a window/grid buffer for TUI programs.  Developed for use in
[Revim](https://github.com/cowboy8625/Revim) a clone of vim remade in Rust.
[eztui](https://github.com/cowboy8625/eztui) a easy way to make clean tui's in Rust


For some examples check the examples folder in project.

